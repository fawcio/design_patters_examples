/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef VIRTUALPROXY_HPP
#define VIRTUALPROXY_HPP
#include <iostream>
#include <memory>

namespace virtual_proxy {

struct Image {
    Image() = default;
    virtual ~Image() = default;

    virtual void draw() = 0;
};

struct Bitmap : Image {
    Bitmap(std::string const& filename)
    {
        std::cout << "Loading bitmap from " << filename << "\n";
    }

    void draw() override { std::cout << "Drawing a bitmap\n"; }
};

struct LazyBitmap : Image {
    LazyBitmap(std::string const& file)
        : filename(file)
    {
    }

    void draw() override
    {
        if (!bmp) {
            bmp = std::make_unique<Bitmap>(filename);
        }

        bmp->draw();
    }

private:
    std::string filename;
    std::unique_ptr<Bitmap> bmp { nullptr };
};

} // namespace virtual_proxy

#endif // VIRTUALPROXY_HPP
