/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef COMMUNICATIONPROXY_HPP
#define COMMUNICATIONPROXY_HPP
#include <chrono>
#include <future>
#include <iostream>

using namespace std::chrono_literals;

namespace communication_proxy {

struct Pingable {
    virtual ~Pingable() = default;

    virtual std::wstring ping(const std::wstring& message) = 0;
};

struct Pong : Pingable {
    std::wstring ping(const std::wstring& message) { return message + L" pong"; }
};

struct RemoteService : Pingable {
    std::string address;
    bool connected { false };

    RemoteService(std::string addr)
        : address(std::move(addr))
    {
    }

    ~RemoteService() { disconnect(); }

    void connect()
    {
        if (!connected) {
            std::cout << "Connecting to " << address << "\n";
            connected = true;
        }
    }

    void disconnect()
    {
        if (connected) {
            std::cout << "Disconnecting from " << address << "\n";
            connected = false;
        }
    }

    std::wstring ping(const std::wstring& message) override
    {
        connect();
        std::wcout << "msg: " << message
                   << " sent to remote, waiting for response... ";
        auto f = std::async([]() {
            std::this_thread::sleep_for(50ms);
            return L"pong";
        });

        return f.get();
    }
};

void tryit(Pingable& pp)
{
    auto response = pp.ping(L"ping");
    std::wcout << " Respnse: " << response << "\n";
}

} // namespace communication_proxy

#endif // COMMUNICATIONPROXY_HPP
