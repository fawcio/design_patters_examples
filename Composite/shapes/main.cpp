/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <memory>
#include <string>
#include <vector>

struct GraphicObject {
    virtual void draw() = 0;

    virtual ~GraphicObject() = default;
};

struct Circle : GraphicObject {
    void draw() override { std::cout << "Circle\n"; }
};

struct Group : GraphicObject {
    std::string name;
    std::vector<std::unique_ptr<GraphicObject>> objects;

    Group(std::string name)
        : name(std::move(name))
    {
    }

    void draw() override
    {
        std::cout << "Group: " << name << " containing: \n";
        for (auto const& obj : objects) {
            std::cout << ' ';
            obj->draw();
        }
    }
};

int main()
{
    Group root("root");
    root.objects.emplace_back(std::make_unique<Circle>());
    root.objects.emplace_back(std::make_unique<Circle>());

    auto childGroup = std::make_unique<Group>("sub-group");
    childGroup->objects.emplace_back(std::make_unique<Circle>());
    childGroup->objects.emplace_back(std::make_unique<Circle>());
    childGroup->objects.emplace_back(std::make_unique<Circle>());

    root.objects.push_back(std::move(childGroup));

    root.draw();
}
