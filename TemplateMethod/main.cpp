/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

class Game
{
public:
    Game(const int players_num) : m_players_number(players_num)
    {
        if (players_num <= 0)
        {
            throw std::invalid_argument{"Players number must be positive"};
        }
    }

    virtual ~Game() = default;

    void run()
    {
        start();

        while (!have_winner())
        {
            take_turn();
        }

        std::cout << "The winner is player " << get_winner() << std::endl;
    }

protected:
    virtual void start() = 0;
    virtual bool have_winner() = 0;
    virtual void take_turn() = 0;
    virtual int get_winner() = 0;

    int next_player()
    {
        if (m_current_player + 1 == m_players_number)
        {
            m_current_player = 0;
        }
        else
        {
            m_current_player += 1;
        }

        return current_player();
    }

    int current_player()
    {
        return m_current_player + 1;
    }

    int m_current_player{0};
    const int m_players_number;
};

class Chess : public Game
{
public:
    Chess() : Game(2) {}

    virtual ~Chess() = default;

protected:
    void start() override
    {
        std::cout << "Chess game starts, it's player " << current_player() << " move..\n";
    }

    bool have_winner() override
    {
        return (m_moves_counter >= 7);
    }

    void take_turn() override
    {
        std::this_thread::sleep_for(10ms);
        std::cout << "Player " << current_player() << " moved, it's player " << next_player() << " move..\n";
        ++m_moves_counter;
    }

    int get_winner() override
    {
        return current_player();
    }

    int m_moves_counter{0};
};

int main()
{
    std::unique_ptr<Game> game = std::make_unique<Chess>();
    game->run();
}