/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef DYNAMIC_HPP
#define DYNAMIC_HPP

#include <iostream>
#include <memory>
#include <sstream>
#include <string>

using namespace std;

namespace Dynamic {

struct Shape {
    virtual std::string str() const = 0;

    virtual ~Shape() = default;
};

struct Circle : public Shape {
    float radius;

    Circle(float r)
        : radius(r)
    {
    }

    void resize(float factor) { radius *= factor; }

    virtual std::string str() const override
    {
        std::stringstream ss;
        ss << "Circle r: " << radius;

        return ss.str();
    }
};

struct Rectangle : public Shape {
    float x, y;

    Rectangle(float x_, float y_)
        : x(x_)
        , y(y_)
    {
    }

    virtual std::string str() const override
    {
        std::stringstream ss;
        ss << "Rectangle x: " << x << " y:" << y;

        return ss.str();
    }
};

// Decorator 1
struct ColoredShape : public Shape {
    std::unique_ptr<Shape> shape;
    std::string color;

    ColoredShape(std::unique_ptr<Shape> s, std::string c)
        : shape(std::move(s))
        , color(std::move(c))
    {
    }

    virtual std::string str() const override
    {
        std::stringstream ss;
        ss << color << ' ' << shape->str();

        return ss.str();
    }
};

// Decorator 2
struct TransparentShape : public Shape {
    std::unique_ptr<Shape> shape;
    int trsp;

    TransparentShape(std::unique_ptr<Shape> s, int t)
        : shape(std::move(s))
        , trsp(t)
    {
    }

    virtual std::string str() const override
    {
        std::stringstream ss;
        ss << shape->str() << " with " << trsp << "% transparency";

        return ss.str();
    }
};

} // namespace Dynamic

#endif // DYNAMIC_HPP
