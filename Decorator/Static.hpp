/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef STATIC_HPP
#define STATIC_HPP

#include <iostream>
#include <memory>
#include <sstream>
#include <string>

using namespace std;

namespace Static {

struct Shape {
    virtual ~Shape() = default;
    virtual std::string str() const = 0;
};

struct Circle : public Shape {
    float radius;

    Circle(float r)
        : radius(r)
    {
    }

    virtual ~Circle() = default;

    void resize(float factor) { radius *= factor; }

    virtual std::string str() const override
    {
        std::stringstream ss;
        ss << "Circle r: " << radius;

        return ss.str();
    }
};

struct Rectangle : public Shape {
    float x, y;

    Rectangle(float x_, float y_)
        : x(x_)
        , y(y_)
    {
    }

    virtual ~Rectangle() = default;

    virtual std::string str() const override
    {
        std::stringstream ss;
        ss << "Rectangle x: " << x << " y:" << y;

        return ss.str();
    }
};

// Static decorator 1
template <typename T>
struct ColoredShape : public T {
    static_assert(is_base_of<Shape, T>::value,
        "Template argument must be a Shape");

    string color;

    // Forwarding constructor
    template <typename... Args>
    ColoredShape(std::string c, Args... args)
        : T(std::forward<Args>(args)...)
        , color(std::move(c))
    {
    }

    std::string str() const override
    {
        std::stringstream ss;
        ss << color << ' ' << T::str();

        return ss.str();
    }
};

// Static decorator 2
template <typename T>
struct TransparentShape : public T {
    static_assert(is_base_of<Shape, T>::value,
        "Template argument must be a Shape");

    int trsp;

    // Forwarding constructor
    template <typename... Args>
    TransparentShape(int t, Args... args)
        : T(std::forward<Args>(args)...)
        , trsp(t)
    {
    }

    std::string str() const override
    {
        std::stringstream ss;
        ss << T::str() << " with " << trsp << "% transparency";

        return ss.str();
    }
};

} // namespace Static

#endif // STATIC_HPP
