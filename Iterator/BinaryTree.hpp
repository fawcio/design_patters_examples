/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef BINARYTREE_HPP
#define BINARYTREE_HPP

#include <iostream>

template <typename T>
struct BinaryTree;

template <typename T>
struct Node {
    T value = T();

    Node<T>* left { nullptr };
    Node<T>* right { nullptr };
    Node<T>* parent { nullptr };

    BinaryTree<T>* tree { nullptr };

    Node(T v)
        : value(std::move(v))
    {
    }

    Node(T v, Node<T>* l, Node<T>* r)
        : value(std::move(v))
        , left(l)
        , right(r)
    {
        if (left) {
            left->tree = tree;
            left->parent = this;
        }
        if (right) {
            right->tree = tree;
            right->parent = this;
        }
    }

    ~Node()
    {
        if (left) {
            delete left;
        }
        if (right) {
            delete right;
        }
    }

    void setTree(BinaryTree<T>* t)
    {
        tree = t;
        if (left) {
            left->setTree(t);
        }
        if (right) {
            right->setTree(t);
        }
    }
};

template <typename T>
struct BinaryTree {
    Node<T>* root { nullptr };

    BinaryTree(Node<T>* r)
        : root(r)
    {
        r->setTree(this);
    }

    template <typename U>
    struct PreOrderIterator {
        Node<U>* current { nullptr };

        PreOrderIterator(Node<U>* c)
            : current(c)
        {
        }

        bool operator!=(PreOrderIterator<U> const& other) const
        {
            return current != other.current;
        }

        PreOrderIterator<U>& operator++()
        {
            if (current->right) {
                current = current->right;
                while (current->left) {
                    current = current->left;
                }
            } else {
                Node<T>* p = current->parent;
                while (p && current == p->right) {
                    current = p;
                    p = p->parent;
                }
                current = p;
            }

            return *this;
        }

        Node<U>& operator*() { return *current; }
    };

    using iterator = PreOrderIterator<T>;

    iterator begin()
    {
        auto n = root;
        if (n) {
            while (n->left) {
                n = n->left;
            }
        }

        return iterator { n };
    }

    iterator end() { return iterator { nullptr }; }
};

#endif // BINARYTREE_HPP
