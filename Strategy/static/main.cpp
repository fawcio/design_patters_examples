/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <vector>

struct MarkdownStrategy
{
    void start(std::ostringstream &) {}
    void end(std::ostringstream &) {}

    void add_list_item(std::ostringstream &oss, const std::string &item)
    {
        oss << "* " << item << std::endl;
    }
};

struct HtmlStrategy
{
    void start(std::ostringstream &oss)
    {
        oss << "<ul>\n";
    }

    void end(std::ostringstream &oss)
    {
        oss << "</ul>\n";
    }

    void add_list_item(std::ostringstream &oss, const std::string &item)
    {
        oss << "  <li>" << item << "</li>\n";
    }
};

template <typename TS>
struct TextProcessor
{
    void append(std::vector<std::string> items)
    {
        list_strategy.start(oss);

        for (auto item : items)
        {
            list_strategy.add_list_item(oss, std::move(item));
        }

        list_strategy.end(oss);
    }

    void clear()
    {
        oss.str("");
        oss.clear();
    }

    void print()
    {
        std::cout << oss.str() << std::endl;
    }

    std::ostringstream oss;
    TS list_strategy;
};

int main()
{
    TextProcessor<MarkdownStrategy> md_processor;

    md_processor.append({"foo", "bar", "baz"});
    md_processor.print();

    TextProcessor<HtmlStrategy> html_processor;
    html_processor.append({"foo", "bar", "baz"});
    html_processor.print();
}