/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <string>

class LightSwitch;

/**
 * @brief The State
 */
struct State {
    virtual ~State() = default;

    virtual void on(LightSwitch* /* ls */)
    {
        std::cout << "Light is already on\n";
    }

    virtual void off(LightSwitch* /* ls */)
    {
        std::cout << "Light is already off\n";
    }
};

struct OnState : State {
    OnState()
    {
        std::cout << "Light turned on\n";
    }

    void off(LightSwitch* ls) override;
};

struct OffState : State {
    OffState()
    {
        std::cout << "Light turned off\n";
    }

    void on(LightSwitch* ls) override;
};

/**
 * @brief the Ligth switch
 */
class LightSwitch {
    State* state;

public:
    LightSwitch()
    {
        state = new OffState();
    }

    void set_state(State* state)
    {
        this->state = state;
    }

    void on() { state->on(this); }
    void off() { state->off(this); }
};

void OnState::off(LightSwitch* ls)
{
    std::cout << "Switching light off...\n";
    ls->set_state(new OffState());
    delete this;
}

void OffState::on(LightSwitch* ls)
{
    std::cout << "Switching light on...\n";
    ls->set_state(new OnState());
    delete this;
}

int main()
{
    LightSwitch ls;
    ls.on();
    ls.off();
    ls.off();
}