/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

enum class State {
    off_hook,
    connecting,
    connected,
    on_hold,
    on_hook
};

std::ostream& operator<<(std::ostream& os, const State& s)
{
    switch (s) {
    case State::off_hook:
        os << "off the hook";
        break;
    case State::connecting:
        os << "connecting";
        break;
    case State::connected:
        os << "connected";
        break;
    case State::on_hold:
        os << "on hold";
        break;
    case State::on_hook:
        os << "on the hook";
        break;
    }

    return os;
}

enum class Trigger {
    call_dialed,
    hung_up,
    call_connected,
    placed_on_hold,
    taken_off_hold,
    left_message,
    stop_using_phone
};

std::ostream& operator<<(std::ostream& os, const Trigger& t)
{
    switch (t) {
    case Trigger::call_dialed:
        os << "call dialed";
        break;
    case Trigger::hung_up:
        os << "hung up";
        break;
    case Trigger::call_connected:
        os << "call connected";
        break;
    case Trigger::placed_on_hold:
        os << "placed on hold";
        break;
    case Trigger::taken_off_hold:
        os << "taken off hold";
        break;
    case Trigger::left_message:
        os << "left message";
        break;
    case Trigger::stop_using_phone:
        os << "putting phone on hook";
        break;
    }

    return os;
}

int main()
{
    using rule_t = std::pair<Trigger, State>;
    std::map<State, std::vector<rule_t>> rules{
        { State::off_hook, { { Trigger::call_dialed, State::connecting }, { Trigger::stop_using_phone, State::on_hook } } },
        { State::connecting, { { Trigger::hung_up, State::off_hook }, { Trigger::call_connected, State::connected } } },
        { State::connected, { { Trigger::left_message, State::off_hook }, { Trigger::hung_up, State::off_hook }, { Trigger::placed_on_hold, State::on_hold } } },
        { State::on_hold, { { Trigger::taken_off_hold, State::connected }, { Trigger::hung_up, State::off_hook } } },
    };

    State current_sate{ State::off_hook };
    State exit_state{ State::on_hook };

    while (true) {
        std::cout << "The phone is " << current_sate << "\n";

    select_trigger:
        std::cout << "Select a trigger:\n";

        int i = 0;
        for (const auto rule : rules[current_sate]) {
            std::cout << i << ". " << rule.first << "\n";
            ++i;
        }

        int input;
        std::cin >> input;

        if (input < 0 || (size_t)input > rules[current_sate].size()) {
            std::cerr << "Incorrect option, pleasew try again.\n";
            goto select_trigger;
        } else {
            current_sate = rules[current_sate].at(input).second;
            if (current_sate == exit_state) {
                break;
            }
        }
    }

    std::cout << "We're done using the phone\n";
}