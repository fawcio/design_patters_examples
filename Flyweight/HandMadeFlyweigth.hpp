/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef HANDMADEFLYWEIGTH_HPP
#define HANDMADEFLYWEIGTH_HPP

#include <boost/bimap.hpp>
#include <iostream>
#include <vector>

using namespace std;

namespace hand_made {

using nameId = uint32_t;

struct User {
    User(string const& f_name, string const& l_name)
        : first_name(add(f_name))
        , last_name(add(l_name))
    {
    }

    string const& firstName() const
    {
        return names.left.find(first_name)->second;
    }

    string const& lastName() const { return names.left.find(last_name)->second; }

    friend ostream& operator<<(ostream& os, const User& user)
    {
        os << "first_name[" << user.first_name << "]: " << user.firstName()
           << " last_name[" << user.last_name << "]: " << user.lastName();
        return os;
    }

protected:
    nameId first_name, last_name;

    static boost::bimap<nameId, string> names;
    static nameId seed;

    static nameId add(string const& name)
    {
        auto it = names.right.find(name);
        if (it == names.right.end()) {
            auto id = seed++;
            names.insert({ id, name });
            return id;
        } else {
            return it->second;
        }
    }
};

nameId User::seed = 0U;
boost::bimap<nameId, string> User::names {};

} // namespace hand_made

#endif // HANDMADEFLYWEIGTH_HPP
