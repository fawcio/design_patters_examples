/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef BITMAPFACADE_HPP
#define BITMAPFACADE_HPP

#include "BitmapTest.hpp"

#include <memory>
#include <vector>

using namespace std;

namespace BitmapFacade {

static void
initGlut(int argc, char** argv)
{
    glutInit(&argc, argv);
}

class Console {
public:
    static Console& instance()
    {
        static Console* console { nullptr };
        if (!console) {
            console = new Console();
        }
        return *console;
    }

    void createWindow(int width, int height, char const* name)
    {
        glutInitWindowSize(width, height);
        windows.push_back(glutCreateWindow(name));

        glutReshapeFunc(bitmap_test::Reshape);
        glutKeyboardFunc(bitmap_test::Key);
        glutDisplayFunc(bitmap_test::Draw);
    }

    void run() { glutMainLoop(); }

private:
    vector<int> windows;

    Console()
    {
        glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
        bitmap_test::Init();
    }
};

} // namespace BitmapFacade

#endif // BITMAPFACADE_HPP
