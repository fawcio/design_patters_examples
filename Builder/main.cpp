/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#include "XMLBuilder.hpp"

#include "GroovyStyleHTMLBuilder.hpp"

#include "PersonAddressBuilder.hpp"
#include "PersonBuilder.hpp"
#include "PersonJobBuilder.hpp"

// clang-format off

int main()
{
  // XML fluent builder test
  XMLElement root = XMLBuilder("ul")
                       .add_child("li", "hello")
                       .add_child("li", "world")
                       .add_child("li", "!");
  XMLElement nestedList = XMLBuilder("ul")
                             .add_child("li", "nested")
                             .add_child("li", "hello")
                             .add_child("li", "world");
  root.add_child(std::move(nestedList));

  nestedList.add_child(XMLBuilder("nested_elem").add_child("key", "value"));

  std::cout << root << std::endl;

  // Grovy style HTML builder test
  std::cout <<
      P {
          Img {"http://pokemon.com/pikachu.png"}
      }
      << std::endl;


  // Person facade builder
  Person p = PersonBuilder()
     .lives().at("123 London Road").with_postcode("SW1 1GB").in("London")
     .works().at("PragmaSoft").as_a("Consultant").earning(10e6);

  // Typical construction
  Person p2;
  p2.street_address = "Tenisowa 23A/2";
  p2.post_code = "71-073";
  p2.city = "Szczecin";
  p2.company_name = "Mobica Limited";
  p2.position = "Senior Software Engineer";
  p2.annual_income = 100'000'000;

  std::cout << p << std::endl;
  std::cout << p2 << std::endl;
}
