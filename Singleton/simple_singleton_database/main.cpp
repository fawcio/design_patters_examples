/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <fstream>
#include <iostream>
#include <map>
#include <mutex>
#include <sstream>
#include <string>
#include <thread>

#include <gtest/gtest.h>

using namespace std::chrono_literals;

template <typename T>
class Singleton {
public:
    Singleton(Singleton const&) = delete;
    Singleton& operator=(Singleton const&) = delete;

    static T& instance()
    {
        static T* instance = nullptr;

        static std::mutex mtx;
        std::scoped_lock<std::mutex> lock(mtx);

        if (nullptr == instance) {
            // Simulate race condition
            std::this_thread::sleep_for(200ms);
            instance = new T();
        }

        return *instance;
    }

protected:
    Singleton() = default;
};

class Database {
public:
    virtual unsigned long populationOf(std::string const& capital) = 0;

    virtual ~Database() = default;
};

class SingletonDatabase
    : public Singleton<SingletonDatabase>,
      public Database {
    friend class Singleton;

    SingletonDatabase()
    {
        std::ifstream ifs { "capitals.txt" };

        std::string name, pop;

        while (std::getline(ifs, name)) {
            std::getline(ifs, pop);

            capitals[name] = std::stoul(pop);
        }
    }

    SingletonDatabase(SingletonDatabase const&) = delete;
    SingletonDatabase& operator=(SingletonDatabase const&) = delete;

    std::map<std::string, unsigned long> capitals;

public:
    unsigned long populationOf(std::string const& capital) override
    {
        return capitals.at(capital);
    }
};

class DummyDatabase : public Database {
public:
    unsigned long populationOf(const std::string&) override { return 666; }
};

// Example where testing is hard
struct SingletonRecorFinder {
    Database& db;

    SingletonRecorFinder(Database& database = SingletonDatabase::instance())
        : db(database)
    {
    }

    unsigned long totalPopulation(std::vector<std::string> const& names)
    {
        unsigned long acc { 0 };
        for (auto const& name : names) {
            // acc += SingletonDatabase::instance().populationOf(name); // Be smart
            // here and replace singleton with mock...
            acc += db.populationOf(name); // Better!
        }

        return acc;
    }
};

TEST(RecordFinderTest, SingletonTotalPopulationTest)
{
    SingletonRecorFinder rf;

    std::vector<std::string> names {
        "Zadupiewo", "ZdupyDoMordyszyn", "Warsaw", "ChujeMuje"
    };
    ASSERT_EQ(9222555U, rf.totalPopulation(names));
}

TEST(RecordFinderTest, SingletonTotalPopulationTest_mocked)
{
    DummyDatabase dummy {};
    SingletonRecorFinder rf { dummy };

    std::vector<std::string> names {
        "Zadupiewo", "ZdupyDoMordyszyn", "Warsaw", "ChujeMuje"
    };
    ASSERT_EQ(4 * 666U, rf.totalPopulation(names));
}

int main(int argc, char* argv[])
{
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
