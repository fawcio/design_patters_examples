/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <boost/di.hpp>
#include <iostream>
#include <memory>

struct IFoo {
    virtual std::string name() = 0;

    virtual ~IFoo() = default;
};

struct Foo : public IFoo {
    static int id;

    Foo() { ++id; }

public:
    std::string name() override
    {
        return std::string("Foo ") + std::to_string(id);
    }
};
int Foo::id = 0;

struct Bar {
    Bar() = delete;
    Bar(Bar const&) = delete;
    Bar& operator=(Bar const&) = delete;
    Bar(Bar&&) = delete;
    Bar& operator=(Bar&&) = delete;

    explicit Bar(std::shared_ptr<IFoo> foo, int x_)
        : fooInstance(foo)
        , x(x_)
    {
        std::cout << "Creating Bar " << x << std::endl;
    }

    std::string name() const { return fooInstance->name(); }

private:
    std::shared_ptr<IFoo> fooInstance;
    int x;
};

class X {
public:
    X() = default;

    int getX() const { return x; }

private:
    int x { 666 };
};

int main()
{
    auto injector = boost::di::make_injector(
        boost::di::bind<IFoo>().to<Foo>().in(boost::di::singleton));

    auto bar1 = injector.create<std::shared_ptr<Bar>>();
    auto bar2 = injector.create<std::shared_ptr<Bar>>();
    auto x = injector.create<X>();

    std::cout << bar1->name() << std::endl;
    std::cout << bar2->name() << std::endl;

    std::cout << x.getX() << std::endl;
}
