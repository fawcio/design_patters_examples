/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "Person.hpp"
#include "ChatRoom.hpp"

#include <iostream>

Person::Person(const string& name)
    : name(name)
{
}

void Person::say(const string& msg) const
{
    room->broadcast(name, msg);
}

void Person::pm(const string& who, const string& msg) const
{
    room->message(name, who, msg);
}

void Person::receive(const string& origin, const string& msg)
{
    const auto text = origin + ": \"" + msg + "\"";
    std::cout << "[" << name << "'s chat session] " << text << std::endl;
    chat_log.emplace_back(std::move(text));
}

bool Person::operator==(const Person& other)
{
    return (name.compare(other.name) == 0);
}

bool Person::operator!=(const Person& other)
{
    return !(*this == other);
}
