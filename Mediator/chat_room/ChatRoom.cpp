/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ChatRoom.hpp"
#include "Person.hpp"
#include <algorithm>

void ChatRoom::broadcast(const string& origin, const string& msg)
{
    for (auto& person : people) {
        if (person->name.compare(origin) != 0) {
            person->receive(origin, msg);
        }
    }
}

void ChatRoom::join(Person* p)
{
    const string join_msg = p->name + " joins the room";
    broadcast("room", join_msg);
    p->room = this;
    people.push_back(p);
}

void ChatRoom::message(const string& origin,
    const string& recipent,
    const string& msg)
{
    auto target = std::find_if(std::begin(people),
        std::end(people),
        [&recipent](const Person* p) { return p->name == recipent; });

    if (target != std::end(people)) {
        (*target)->receive(origin, msg);
    }
}