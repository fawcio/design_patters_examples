/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>

struct File;
struct Document;

// Wrong, complex interface
struct IMFMachine {
    virtual void print(Document const& doc) = 0;
    virtual void scan(File const& srcFile, Document& doc) = 0;
    virtual void fax(Document const& doc) = 0;

    virtual ~IMFMachine() = default;
};

// Better, segregated interfaces
struct IPrinter {
    virtual void print(Document const& doc) = 0;

    virtual ~IPrinter() = default;
};

struct IScanner {
    virtual void scan(File const& srcFile, Document& doc) = 0;

    virtual ~IScanner() = default;
};

struct IFaxMachine {
    virtual void fax(Document const& doc) = 0;

    virtual ~IFaxMachine() = default;
};

struct MFP : public IMFMachine {
public:
    void print(const Document&) override
    {
        // ok, it's a printer
    }

    void scan(const File&, Document&) override
    {
        // ok, it's a scanner
    }

    void fax(const Document&) override
    {
        // ok, it's a fax machine
    }
};

struct WickedScanner : public IMFMachine {
public:
    void print(const Document&) override
    {
        // not ok! it's not a printer
    }
    void scan(const File&, Document&) override
    {
        // ok, it's a scanner
    }
    void fax(const Document&) override
    {
        // not ok! it's not a fax machine
    }
};

struct NormalScanner : public IScanner {
public:
    void scan(const File&, Document&) override
    {
        // ok, it's just a scanner
    }
};

int main()
{
}
