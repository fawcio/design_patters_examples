/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <string>

#include "MathiWithVariablesInterpreter.hpp"
#include "SimpleMathInterpreter.hpp"

int main()
{
    // Simple Math interpreter test
    try {
        std::string input { "(13-4)-(12+1)" };

        auto tokens = simple_interpreter::lex(input);
        auto expression = simple_interpreter::parse(tokens);

        std::cout << input << " = " << expression->eval() << "\n";
    } catch (std::exception& e) {
        std::cerr << "Unhandled exception: " << e.what() << std::endl;
    }

    // Math with variables interpreter test

    auto processor = ExpressionProcessor {};
    processor.variables['x'] = 3;

    std::string input { "10-6+x" };
    std::cout << input << " = " << processor.calculate(input) << "\n";
}
