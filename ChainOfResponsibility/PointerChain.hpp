/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef POINTERCHAIN_HPP
#define POINTERCHAIN_HPP
#include <iostream>

namespace pointer_chain {

struct Creature {
    std::string name;
    int attack, defense;

    Creature(std::string const name, int const attack, int const defense)
        : name(std::move(name))
        , attack(attack)
        , defense(defense)
    {
    }

    friend std::ostream& operator<<(std::ostream& os, Creature const& c)
    {
        os << "Creature: " << c.name << " [att: " << c.attack
           << ", def: " << c.defense << "]\n";
        return os;
    }
};

class CreatureModifier {
    CreatureModifier* next { nullptr };

protected:
    Creature& creature;

public:
    CreatureModifier(Creature& c)
        : creature(c)
    {
    }

    virtual ~CreatureModifier() = default;

    void add(CreatureModifier& cm)
    {
        if (next)
            next->add(cm);
        else
            next = &cm;
    }

    virtual void handle()
    {
        if (next) {
            next->handle();
        }
    }
};

class DoubleAttackModifier : public CreatureModifier {
public:
    DoubleAttackModifier(Creature& cr)
        : CreatureModifier(cr)
    {
    }

    virtual ~DoubleAttackModifier() = default;

    void handle() override
    {
        creature.attack *= 2;
        CreatureModifier::handle(); //!!!11
    }
};

class IncreasedDefenceModifier : public CreatureModifier {
public:
    IncreasedDefenceModifier(Creature& cr)
        : CreatureModifier(cr)
    {
    }

    virtual ~IncreasedDefenceModifier() = default;

    void handle() override
    {
        creature.defense += 15;
        CreatureModifier::handle(); //!!!!11
    }
};

class NoBonusModifier : public CreatureModifier {
public:
    NoBonusModifier(Creature& cr)
        : CreatureModifier(cr)
    {
    }

    virtual ~NoBonusModifier() = default;

    void handle() override
    {
        // break chain of responsibility
    }
};

} // namespace pointer_chain

#endif // POINTERCHAIN_HPP
