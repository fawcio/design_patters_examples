/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Sławomir Cielepak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>

#include "BrokerChain.hpp"
#include "PointerChain.hpp"

int main()
{
    // Pointer chain test
    pointer_chain::Creature goblin { "Goblin", 7, 1 };
    std::cout << goblin;

    pointer_chain::CreatureModifier root { goblin };
    pointer_chain::DoubleAttackModifier r1 { goblin };
    pointer_chain::DoubleAttackModifier r1_2 { goblin };
    pointer_chain::IncreasedDefenceModifier r2 { goblin };

    pointer_chain::NoBonusModifier curse { goblin };

    root.add(r1);
    root.add(r1_2);
    root.add(curse); // break the chain of resp.
    root.add(r2);

    root.handle();

    std::cout << goblin;

    // Broker Chain test
    broker::Game game;
    broker::Creature ogre { game, "Ogre", 1, 1 };
    {
        broker::IncreasedDefenseModifier idm1 {
            game, ogre
        }; // As long as modifiers live, ogre is stronger!
        std::cout << ogre << "\n";

        {
            broker::DoubleAttackModifier dam { game, ogre };
            broker::IncreasedDefenseModifier idm2 { game, ogre };
            std::cout << ogre << "\n";
        }
    }

    std::cout << ogre << "\n"; // Ogre is lame again..
}
